import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SurveydetailPage } from '../pages/surveydetail/surveydetail';
import { SurveyserviceProvider } from '../providers/surveyservice/surveyservice';
import { HttpClientModule } from '@angular/common/http';
import { SharedGlobalProvider } from '../providers/shared-global/shared-global';
import { RegisterationPage } from '../pages/registeration/registeration';
import { UserserviceProvider } from '../providers/userservice/userservice';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ForgotPage } from '../pages/forgot/forgot';
import { SubmissionlistPage } from '../pages/submissionlist/submissionlist';
import { SurveyEditPage } from '../pages/survey-edit/survey-edit';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { IonicStorageModule } from '@ionic/storage';
import { ThankyouPage } from '../pages/thankyou/thankyou';


@NgModule({
  declarations: [
    MyApp,
    SurveydetailPage,
    TabsPage,
    RegisterationPage,
    HomePage,
    LoginPage,
    ForgotPage,
    SubmissionlistPage,
    SurveyEditPage,
    ChangePasswordPage,
    ThankyouPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SurveydetailPage,
    TabsPage,
    RegisterationPage,
    HomePage,
    LoginPage,
    ForgotPage,
    SubmissionlistPage,
    SurveyEditPage,
    ChangePasswordPage,
    ThankyouPage
  ],
  providers: [
    HttpClientModule,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SurveyserviceProvider,
    SharedGlobalProvider,
    UserserviceProvider
  ]
})
export class AppModule {}
