import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from 'ionic-angular';

@Injectable()
export class SharedGlobalProvider {
  //baseUrl='http://blog.ambriontech.com/api/';
  //baseUrl='http://localhost/survey-app/api/';
  baseUrl='https://lsdadb.com/api/';
  private token='';
  private userId:number;
  private surveyTabClick:number=0;
  constructor(public http: HttpClient,public toastCtrl:ToastController,
    public loadingCtrl:LoadingController) {}

  setToken(tokenValue:string){
    this.token=tokenValue;
  }

  setUserId(userId:number){
    return this.userId=userId;
  }

  getSurveyOpened(){
    return this.surveyTabClick;
  }

  setSurveyOpened(index:number){
    this.surveyTabClick=index;
  }

  getUserId(){
    return this.userId;
  }

  getToken(){
    return this.token;
  }

  setNotifyMessage(msg:string,duration:number,typeClass:string){
    let toast=this.toastCtrl.create({
      message: msg,
      duration: duration,
      position: 'top',
      cssClass: typeClass,
    });
    toast.present();
  }

  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present();
    return loader;
  }



}
