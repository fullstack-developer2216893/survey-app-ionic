import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { SharedGlobalProvider } from '../../providers/shared-global/shared-global';
import { ForgotPage } from '../forgot/forgot';
import { TabsPage } from '../tabs/tabs';
import { SurveydetailPage } from '../surveydetail/surveydetail';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  private loginForm:FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private formBuilder: FormBuilder,private userService:UserserviceProvider,
    private globalService:SharedGlobalProvider,private storage: Storage) {
      this.loginForm=this.formBuilder.group({
        email:['',Validators.required],
        password:['',Validators.required]
      });
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  submitLogForm() {
    let loading=this.globalService.presentLoading();
    this.userService.loginUser(this.loginForm.value).subscribe(
      (response)=>{
          this.globalService.setToken(response['token']);
          this.storage.set('user-token', response['token']);
          this.globalService.setUserId(response['userId']);
          this.storage.set('user-id', response['userId']);
          this.globalService.setNotifyMessage('User logged in successfully',2000,'success-toastr');
          loading.dismiss();
          this.globalService.setSurveyOpened(1);
          this.navCtrl.push(SurveydetailPage);
          this.navCtrl.setRoot(TabsPage);
          
      },
      (error:Error)=>{
        let errorMessage='Please enter valid detail'; 
        if(error.message=='Http failure response for (unknown url): 0 Unknown Error'){
          errorMessage='Internet is not connected';
        }
        this.globalService.setNotifyMessage(errorMessage,2000,'error-toastr');
        loading.dismiss();
      }
    );
  }

  openPage(pageName:string){
    switch(pageName){
      case 'forgot':
      this.navCtrl.push(ForgotPage);
      break;
    }
  }
}
