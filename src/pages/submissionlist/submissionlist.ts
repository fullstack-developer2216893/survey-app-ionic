import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import { SharedGlobalProvider } from '../../providers/shared-global/shared-global';
import { SurveyserviceProvider } from '../../providers/surveyservice/surveyservice';
import { SurveyModel } from '../../shared/survey.model';
import { SurveyEditPage } from '../survey-edit/survey-edit';
import { LoginPage } from '../login/login';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the SubmissionlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-submissionlist',
  templateUrl: 'submissionlist.html',
})
export class SubmissionlistPage {
  userSubmissionArr:SurveyModel[]=[];
  submissionNoArr=[];
  submittedArr=[];
  percentage=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,
            private globalService:SharedGlobalProvider,private surveyService:SurveyserviceProvider,
            private storage: Storage) {
              
  }

  ionViewWillEnter() {
    if(this.globalService.getUserId()){
      this.globalService.setSurveyOpened(1);
      let loader=this.globalService.presentLoading();
      this.surveyService.getuserSurveys(this.globalService.getUserId()).subscribe(
        (response:any[])=>{
          if(response){
            this.userSubmissionArr=[];
            this.submissionNoArr=[];
            this.percentage=[];
            this.submittedArr=[];
            for(let item of response){
              this.userSubmissionArr.push(
                new SurveyModel(item.survey.id,item.answer,item.survey.created_at,
                  item.survey.update_at)
              );
              this.submissionNoArr.push(item.submission_no);
              this.percentage.push(item.percentage);
              this.submittedArr.push(item.last_question);
            }
            
          }
          loader.dismiss();
        },
        (error:Error)=>{
          if(error['error'][0]=='token_expired'){
            this.storage.set('user-token', '');
            this.storage.set('user-id', '');
            this.globalService.setNotifyMessage('Sorry, your login has expired',2000,'error-toastr');
           }else{
            this.storage.set('user-id', '');
            this.storage.set('user-token', '');
            this.globalService.setNotifyMessage('An unknown error occurred, please try again.',2000,'error-toastr');
          }
          loader.dismiss();
          this.navCtrl.push(LoginPage);
        }
      );
    }
  }

  openDetailPage(survey:SurveyModel,index:number){
      this.navCtrl.push(SurveyEditPage,{
        id:survey.id,
        sno:this.submissionNoArr[index],
        last_question:this.submittedArr[index],
        percentage:this.percentage[index],
        }); 
  }

}