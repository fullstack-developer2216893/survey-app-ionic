import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurveyEditPage } from './survey-edit';

@NgModule({
  declarations: [
    SurveyEditPage,
  ],
  imports: [
    IonicPageModule.forChild(SurveyEditPage),
  ],
})
export class SurveyEditPageModule {}
