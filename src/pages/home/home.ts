import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { RegisterationPage } from '../registeration/registeration';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  rootPage:any=TabsPage;
  constructor(public navCtrl: NavController) {
  }
  
  openPage(pageName:string){
    switch(pageName){
      case 'login':
      this.navCtrl.push(LoginPage);
      break;
      default:
      this.navCtrl.push(RegisterationPage);
      break;
    }
  }
}


