import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SharedGlobalProvider } from '../../providers/shared-global/shared-global';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { LoginPage } from '../login/login';

/**
 * Generated class for the ForgotPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot',
  templateUrl: 'forgot.html',
})
export class ForgotPage {
  private forgotForm:FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              private formBuilder:FormBuilder,private globalService:SharedGlobalProvider,
            private userService:UserserviceProvider) {
    this.forgotForm=this.formBuilder.group({
      email:['',Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPage');
  }

  submitForm() {
    let loading=this.globalService.presentLoading();
    this.userService.forgotUser(this.forgotForm.value).subscribe(
      (response)=>{
          this.globalService.setNotifyMessage(response['message'],2000,'success-toastr');
          loading.dismiss();
          this.navCtrl.push(LoginPage);
      },
      (error:Error)=>{
        this.globalService.setNotifyMessage('Please enter valid email',2000,'error-toastr');
        loading.dismiss();
      }
    );
  }

}
